# 2D 3DoF planar robot arm simulator in Matlab

Matlab project containing 2-dimensional Inverse Kinematics (IK) computation and Forward Kinematics (FK) visualization for a planar 3 Degree-of-Freedom (DoF) robot arm.  
IK computed using the Pseudo-inverse Jacobian method with the exception of initial angles which are computed geometrically.

## Getting Started

### Prerequisites

Last tested on Matlab R2018a.

Required toolboxes:-  
1. Symbolic Math Toolbox (for ik_2d_3dof.m and fk_2d_3dof.m)  
2. Signal Processing Toolbox (for obtaining a and b used in Butterworth filter in test3.m)

Standalone project should not require any toolboxes.

### Installing

Get and install your latest distribution of Matlab

## Running the program

Running main_GUI.m in the standalone project folder will display the GUI for the latest project build. The demos will behave like below

![Screen](traj_IK_demos.gif)

filtNoPhaseDist.m is a manual implementation of Matlab's filtfilt.m which requires the Signal Processing Toolbox.

fk_2d_3dof.m is the prototype script that uses the Symbolic Math Toolbox, requires ik_2d_3dof.m.

test3.m is the non-GUI prototype script of main_GUI.m, requires  
filtNoPhaseDist.m,  
cubicHermSpline.m,  
FKvis_3dof.m and  
IKsolver_3dof.m  
which are in the standalone project folder with the exception of filtNoPhaseDist.m.

## Versioning

1.0.1

## Authors

* **'Alez' TAN Wei Xin**

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

This project is licensed under the **[MIT license](http://opensource.org/licenses/mit-license.php)**

## Acknowledgments

* Xiao, Sun (https://waseda.pure.elsevier.com/en/persons/xiao-sun/publications/) for providing the template and explaining the IK pseudo-inverse method
* All other people and papers cited in the code

