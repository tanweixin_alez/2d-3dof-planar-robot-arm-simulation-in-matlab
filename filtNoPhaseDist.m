function fltd = filtNoPhaseDist(b, a, cmplx)
    % filtNoPhaseDist - Filters complex signal data based on given transfer 
    %                   function coefficients with no phase distortion
    % Based on MATLAB's own filtfilt.m
    %
    % Syntax:  fltd = filtNoPhaseDist(b, a, cmplx)
    %
    % Inputs:
    %    b      - Numerator coefficients of rational transfer function as 
    %               a row vector, see https://www.mathworks.com/help/matlab/ref/filter.html
    %    a      - Denominator coefficients of rational transfer function as 
    %               a row vector, see https://www.mathworks.com/help/matlab/ref/filter.html
    %    cmplx  - Complex signal as a 1D column
    %               i.e. [ x0 + i*y0; x1 + i*y1; ...] for xy coordinates
    %
    % Outputs:
    %    fltd   - Filtered signal as a 1D column
    %               i.e. [ x0 + i*y0; x1 + i*y1; ...] for xy coordinates
    %
    % Example:
    %   x = [x0 x1 ...]; y = [y0 y1 ...];
    %   cmplx_sgnl = transpose(complex(x,y));
    %   b_coeff = [5.97957803699783e-05,0.000298978901849892,0.000597957803699784,0.000597957803699784,0.000298978901849892,5.97957803699783e-05];
    %   a_coeff = [1,-3.98454311961234,6.43486709027587,-5.25361517035227,2.16513290972413,-0.359928245063557];
    %   fltd_sgnl = filtNoPhaseDist(b_coeff,a_coeff,cmplx_sgnl);
    %   fltd_xy = [transpose(real(fltd_sgnl));transpose(imag(fltd_sgnl))];
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: filter.m, filtfilt.m
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 10-Arpil-2019

    %------------- BEGIN CODE --------------        
    b = transpose(b); a = transpose(a); % convert coefficients into column form
    nfilt = max(numel(b),numel(a)); % determine filter order  
    nfact = max(1,3*(nfilt-1));  % determine length of edge transients
    
    % Compute initial conditions to remove DC offset at beginning and end of
    % filtered sequence.  Use sparse matrix to solve linear system for initial
    % conditions zi, which is the vector of states for the filter b(z)/a(z) in
    % the state-space formulation of the filter.
    rows = [1:nfilt-1, 2:nfilt-1, 1:nfilt-2];
    cols = [ones(1,nfilt-1), 2:nfilt-1, 2:nfilt-1];
    vals = [1+a(2), a(3:nfilt).', ones(1,nfilt-2), -ones(1,nfilt-2)];
    rhs  = b(2:nfilt) - b(1)*a(2:nfilt);
    zi   = sparse(rows,cols,vals) \ rhs;

    % Single channel, data explicitly concatenated into one vector
    fltd = [2*cmplx(1)-cmplx(nfact+1:-1:2); cmplx; 2*cmplx(end)-cmplx(end-1:-1:end-nfact)];

    % filter, reverse data, filter again, and reverse data again
    fltd = filter(b(:,1),a(:,1),fltd,zi(:,1)*fltd(1));
    fltd = fltd(end:-1:1);
    fltd = filter(b(:,1),a(:,1),fltd,zi(:,1)*fltd(1));

    % retain reversed central section of y
    fltd = fltd(end-nfact:-1:nfact+1);
    %------------- END OF CODE --------------
end