close all;
clear;

%% Link lengths must always have 3 columns and 1 row since program was made fo a 3DoF planar robot
% No time to implement variable number of links so change only the values
link_lens = [ 1.0, 1.0, 1.0];

%% Graphical input for saving user provided waypoints to be used in generating robot trajectory
% Prompt for determining number of waypoints for graphical input
prompt = 'How many waypoints for trajectory? (Input integer value please)\n';
no_of_waypts = input(prompt);

% Setting up figure for plotting trajectory
h_fig = figure(1);
hold on
h_fig = setup_fig(link_lens, h_fig);

% Using ginput() to save user mouse clicks as waypoints
waypt_coord = zeros(no_of_waypts,2);
n = 0;
while n < no_of_waypts
    
    fprintf('Please place waypoint, valid workspace is between the magenta lines\n')
    [x,y,click_flag1] = ginput(1);
    if isempty(x) || click_flag1(1) ~= 1
        break;
    end
    
    % Prevents points not within the robot's workspace from being selected
    if (x^2 + y^2) > (sum(link_lens)^2) || ...
    ( (sum(link_lens(2:end)) < link_lens(1)) && ((x^2 + y^2) < ((link_lens(1)-sum(link_lens(2:end)))^2)) )
        fprintf('Waypoint is not in valid workspace, please try again\n')
        fprintf(['Current no. of input waypoints: ',num2str(n),'\n\n'])
    else
        % Updates user on status of graphical input process
        n = plus(n,1);
        fprintf(['Current no. of input waypoints: ',num2str(n),'\n'])
        fprintf('Press enter if you want to terminate waypoint input earlier\n\n')
        waypt_coord(n,:) = [x,y];
        plot(x,y,'rx','LineWidth',2)
        drawnow  
    end
    
end

%% Generating trajectory from waypoints obtained previously
traj_coord = cubicHermSpline(no_of_waypts, waypt_coord, 1000);

%% Filtering the trajectory using a 5th order butterworth lowpass filter to smooth velocity and acceleration values
% Filter transfer function coefficients (a,b) obtained from 
% http://www-users.cs.york.ac.uk/~fisher/mkfilter/trad.html
% for a normalized cutoff frequency of w = 0.1
b_coeff = [5.97957803699783e-05,0.000298978901849892,0.000597957803699784,0.000597957803699784,0.000298978901849892,5.97957803699783e-05];
a_coeff = [1,-3.98454311961234,6.43486709027587,-5.25361517035227,2.16513290972413,-0.359928245063557];
cmplx_sgnl = transpose(complex(traj_coord(1,:),traj_coord(2,:)));
fltd_sgnl = filtNoPhaseDist(b_coeff,a_coeff,cmplx_sgnl);
fltd_traj_coord = [transpose(real(fltd_sgnl));transpose(imag(fltd_sgnl))];

% Plotting of filtered trajectory on figure
plot(fltd_traj_coord(1,:),fltd_traj_coord(2,:),'b')

%% Determine the joint angles of the robot based on trajectory coordinates
joint_angles_rad = IKsolver_3dof(fltd_traj_coord,link_lens);

%% Visualize the robot's motion based on the computed joint angles
FKvis_3dof(joint_angles_rad,link_lens,50);

hold off;
pause off;

function fig_handle = setup_fig(link_lengths, fig_handle)

    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % setting appropriate axis limits for figure based on sum of link
    % % lengths rounded up to largest integer
    x_limits = [-ceil(sum(link_lengths)) ceil(sum(link_lengths))];
    xlim(x_limits);
    line(x_limits, [0 0],'Color','k');
    y_limits = [-ceil(sum(link_lengths)) ceil(sum(link_lengths))];
    ylim(y_limits);
    line([0 0], y_limits,'Color','k');
    
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % determining robot workspace and plotting in figure
    % % in case of 2D planar robot, workspace is in a shape of a circle
    th = linspace(0,2*pi,100);
    crc_outer = [sum(link_lengths) * cos(th); sum(link_lengths) * sin(th)];
    plot(crc_outer(1,:),crc_outer(2,:),'m')
    % % inner circle limits only applicable when length of link2 + link3 
    % % is less than that of link1, resulting in a donut shape
    if sum(link_lengths(2:end)) < link_lengths(1)
        r_inner = link_lengths(1)-sum(link_lengths(2:end));
        crc_inner = [r_inner * cos(th); r_inner * sin(th)];
        plot(crc_inner(1,:),crc_inner(2,:),'m')
    end
    
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % other relevant figure settings
    set(fig_handle, 'Position', [300, 0, 1024, 768]);
    pause on;
    grid on;
    axis equal;

end