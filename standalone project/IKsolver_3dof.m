function th_out = IKsolver_3dof(traj_coord, link_lengths)
    % IKsolver_3dof - Uses the pseudoinverse of the robot's Jacobian matrix
    %                   to numerically solve for the appropriate joint
    %                   angles that correspond to the given trajectory
    %                   coordinates, exception is for the initial joint
    %                   angles which are determined geometrically
    % Pseudoinverse numerical solution based on http://www.andreasaristidou.com/publications/papers/CUEDF-INFENG,%20TR-632.pdf
    % Initial angles are based on https://www.sciencedirect.com/science/article/pii/S1569190X10001772
    %
    % Syntax:  th_out = IKsolver_3dof(traj_coord, link_lengths)
    %
    % Inputs:
    %    traj_coord     - xy coordinates of points along trajectory as
    %                       column vectors i.e. [x0 x1 ...; y0 y1 ...]
    %    link_lengths   - length of robot links as 1D row i.e. [l1 l2 l3]
    %
    % Outputs:
    %    th_out         - joint angle values for input trajectory in
    %                       radians as column vectors i.e. [th1_1 th1_2 
    %                       ...; th2_1 th2_2 ...; th3_1 th3_2 ...]
    %
    % Example:
    %    link_lens = [l1 l2 l3];
    %    traj = [x0 x1 ...; y0 y1 ...];
    %    joint_angles_rad = IKsolver_3dof(traj,link_lens);
    %
    % Other m-files required: none
    % Subfunctions:           newton_method
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Set up equations as string to be converted into anonymous function for
    % numerical computation using Newton's method as seen in
    % https://www.sciencedirect.com/science/article/pii/S1569190X10001772
    s_str1 = num2str(link_lengths(1));
    s_str2 = strcat(num2str(link_lengths(2)),'*sin(th)');
    for i = 2:size(link_lengths,2)
        s_str1 = strcat(s_str1,'+',num2str(link_lengths(i)),'*cos(',num2str(i-1),'*th)');
        if i < size(link_lengths,2)
            s_str2 = strcat(s_str2,'+',num2str(link_lengths(i+1)),'*sin(',num2str(i),'*th)');
        end
    end
    s_str = strcat('(',s_str1,')^2+(',s_str2,')^2-',num2str(traj_coord(1,1)^2 + traj_coord(2,1)^2));
    % Convert string into anonymous function and solve numerically using
    % newton's method
    s_func = str2func(strcat('@(th)',s_str));
    theta_rad = newton_method(s_func,1.0,1e-3,1e4); % Solution for theta used to find gamma
    
    gamma = atan2(traj_coord(2,1),traj_coord(1,1));
    gamma_x = strcat(num2str(link_lengths(1)),'*cos(a)');
    gamma_y = strcat(num2str(link_lengths(1)),'*sin(a)');
     for i = 2:size(link_lengths,2)
        gamma_x = strcat(gamma_x,'+',num2str(link_lengths(i)),'*cos(a+',num2str(i-1),'*',num2str(theta_rad),')');
        gamma_y = strcat(gamma_y,'+',num2str(link_lengths(i)),'*sin(a+',num2str(i-1),'*',num2str(theta_rad),')');
    end
    gamma_str = strcat('atan2(',gamma_y,',',gamma_x,')-',num2str(gamma));
    % Convert string into anonymous function and solve numerically using
    % newton's method
    gamma_func = str2func(strcat('@(a)',gamma_str));
    alpha_rad = newton_method(gamma_func,1.0,1e-3,1e4);
    
    % Set solutions to theta and alpha as initial joint angles
    th_out = zeros(3,size(traj_coord,2));
    th_out(:,1) = [alpha_rad; theta_rad; theta_rad];

    % Pseudoinverse of the Jacobian of the robot's forward kinematic matrix
    % pre-computed analytically and hardcoded as anonymous functions
    J_psinv_11 = @(th1,th2,th3,l1,l2,l3)    -(l2*l3^2*sin(th1 + th2) - l2*l3^2*sin(th1 + th2 + 2*th3) + l1*l2^2*sin(th1) + 2*l1*l3^2*sin(th1) - 2*l1*l3^2*sin(th1 + 2*th2 + 2*th3) - l1*l2^2*sin(th1 + 2*th2) - 2*l1*l2*l3*sin(th1 + 2*th2 + th3) + l1*l2*l3*sin(th1 + th3) + l1*l2*l3*sin(th1 - th3))/(l1^2*l2^2 + 2*l1^2*l3^2 + 2*l2^2*l3^2 - l1^2*l2^2*cos(2*th2) - 2*l2^2*l3^2*cos(2*th3) - 2*l1^2*l3^2*cos(2*th2 + 2*th3) - 2*l1*l2*l3^2*cos(th2 + 2*th3) - 2*l1^2*l2*l3*cos(2*th2 + th3) + 2*l1*l2*l3^2*cos(th2) + 2*l1^2*l2*l3*cos(th3));
    J_psinv_12 = @(th1,th2,th3,l1,l2,l3)     (l2*l3^2*cos(th1 + th2) - l2*l3^2*cos(th1 + th2 + 2*th3) + l1*l2^2*cos(th1) + 2*l1*l3^2*cos(th1) - 2*l1*l3^2*cos(th1 + 2*th2 + 2*th3) - l1*l2^2*cos(th1 + 2*th2) - 2*l1*l2*l3*cos(th1 + 2*th2 + th3) + l1*l2*l3*cos(th1 + th3) + l1*l2*l3*cos(th1 - th3))/(l1^2*l2^2 + 2*l1^2*l3^2 + 2*l2^2*l3^2 - l1^2*l2^2*cos(2*th2) - 2*l2^2*l3^2*cos(2*th3) - 2*l1^2*l3^2*cos(2*th2 + 2*th3) - 2*l1*l2*l3^2*cos(th2 + 2*th3) - 2*l1^2*l2*l3*cos(2*th2 + th3) + 2*l1*l2*l3^2*cos(th2) + 2*l1^2*l2*l3*cos(th3));
    J_psinv_21 = @(th1,th2,th3,l1,l2,l3)    -(l1^2*l3*sin(th2 - th1 + th3) - l2*l3^2*sin(th1 + th2 + 2*th3) + l1^2*l2*sin(th1 + th2) + l2*l3^2*sin(th1 + th2) - l1*l2^2*sin(th1) - l1*l3^2*sin(th1) + l1*l3^2*sin(th1 + 2*th2 + 2*th3) - l1^2*l2*sin(th1 - th2) + l1*l2^2*sin(th1 + 2*th2) + l1^2*l3*sin(th1 + th2 + th3) + 2*l1*l2*l3*sin(th1 + 2*th2 + th3) - l1*l2*l3*sin(th1 + th3) - l1*l2*l3*sin(th1 - th3))/(l1^2*l2^2 + 2*l1^2*l3^2 + 2*l2^2*l3^2 - l1^2*l2^2*cos(2*th2) - 2*l2^2*l3^2*cos(2*th3) - 2*l1^2*l3^2*cos(2*th2 + 2*th3) - 2*l1*l2*l3^2*cos(th2 + 2*th3) - 2*l1^2*l2*l3*cos(2*th2 + th3) + 2*l1*l2*l3^2*cos(th2) + 2*l1^2*l2*l3*cos(th3));
    J_psinv_22 = @(th1,th2,th3,l1,l2,l3)    -(l1^2*l3*cos(th2 - th1 + th3) + l2*l3^2*cos(th1 + th2 + 2*th3) - l1^2*l2*cos(th1 + th2) - l2*l3^2*cos(th1 + th2) + l1*l2^2*cos(th1) + l1*l3^2*cos(th1) - l1*l3^2*cos(th1 + 2*th2 + 2*th3) + l1^2*l2*cos(th1 - th2) - l1*l2^2*cos(th1 + 2*th2) - l1^2*l3*cos(th1 + th2 + th3) - 2*l1*l2*l3*cos(th1 + 2*th2 + th3) + l1*l2*l3*cos(th1 + th3) + l1*l2*l3*cos(th1 - th3))/(l1^2*l2^2 + 2*l1^2*l3^2 + 2*l2^2*l3^2 - l1^2*l2^2*cos(2*th2) - 2*l2^2*l3^2*cos(2*th3) - 2*l1^2*l3^2*cos(2*th2 + 2*th3) - 2*l1*l2*l3^2*cos(th2 + 2*th3) - 2*l1^2*l2*l3*cos(2*th2 + th3) + 2*l1*l2*l3^2*cos(th2) + 2*l1^2*l2*l3*cos(th3));
    J_psinv_31 = @(th1,th2,th3,l1,l2,l3)    -(l3*(l1^2*sin(th1 + th2 + th3) + 2*l2^2*sin(th1 + th2 + th3) + l1^2*sin(th2 - th1 + th3) - 2*l2^2*sin(th1 + th2 - th3) - l1*l3*sin(th1) + l1*l3*sin(th1 + 2*th2 + 2*th3) - 2*l1*l2*sin(th1 - th3) + l1*l2*sin(th1 + 2*th2 + th3) + 2*l2*l3*sin(th1 + th2 + 2*th3) + l1*l2*sin(th1 + th3) - 2*l2*l3*sin(th1 + th2)))/(l1^2*l2^2 + 2*l1^2*l3^2 + 2*l2^2*l3^2 - l1^2*l2^2*cos(2*th2) - 2*l2^2*l3^2*cos(2*th3) - 2*l1^2*l3^2*cos(2*th2 + 2*th3) - 2*l1*l2*l3^2*cos(th2 + 2*th3) - 2*l1^2*l2*l3*cos(2*th2 + th3) + 2*l1*l2*l3^2*cos(th2) + 2*l1^2*l2*l3*cos(th3));
    J_psinv_32 = @(th1,th2,th3,l1,l2,l3)     (l3*(l1^2*cos(th1 + th2 + th3) + 2*l2^2*cos(th1 + th2 + th3) - l1^2*cos(th2 - th1 + th3) - 2*l2^2*cos(th1 + th2 - th3) - l1*l3*cos(th1) + l1*l3*cos(th1 + 2*th2 + 2*th3) - 2*l1*l2*cos(th1 - th3) + l1*l2*cos(th1 + 2*th2 + th3) + 2*l2*l3*cos(th1 + th2 + 2*th3) + l1*l2*cos(th1 + th3) - 2*l2*l3*cos(th1 + th2)))/(l1^2*l2^2 + 2*l1^2*l3^2 + 2*l2^2*l3^2 - l1^2*l2^2*cos(2*th2) - 2*l2^2*l3^2*cos(2*th3) - 2*l1^2*l3^2*cos(2*th2 + 2*th3) - 2*l1*l2*l3^2*cos(th2 + 2*th3) - 2*l1^2*l2*l3*cos(2*th2 + th3) + 2*l1*l2*l3^2*cos(th2) + 2*l1^2*l2*l3*cos(th3));
    
    % Determine the velocity between points on trajectory for use in
    % numerical computation
    dt = 1/size(traj_coord,2);
    traj_vel = gradient(traj_coord,dt);

    for i = 1:size(traj_vel,2)-1
        
        if (traj_coord(1,i)^2 + traj_coord(2,i)^2) > (sum(link_lengths)^2) || ...
    ( (sum(link_lengths(2:end)) < link_lengths(1)) && ((traj_coord(1,i)^2 + traj_coord(2,i)^2) < ((link_lengths(1)-sum(link_lengths(2:end)))^2)) )
            % Assign joint angles as NaN if trajectory not within robot
            % workspace
            th_out(:,i+1) = NaN(size(th_out(:,i)));
        else
            % Numerically determine Jacobian pseudoinverse matrix for every 
            % timestep
            J_psinv_num = [   J_psinv_11(th_out(1,i),th_out(2,i),th_out(3,i),link_lengths(1),link_lengths(2),link_lengths(3)), J_psinv_12(th_out(1,i),th_out(2,i),th_out(3,i),link_lengths(1),link_lengths(2),link_lengths(3))  ;
                              J_psinv_21(th_out(1,i),th_out(2,i),th_out(3,i),link_lengths(1),link_lengths(2),link_lengths(3)), J_psinv_22(th_out(1,i),th_out(2,i),th_out(3,i),link_lengths(1),link_lengths(2),link_lengths(3))  ;
                              J_psinv_31(th_out(1,i),th_out(2,i),th_out(3,i),link_lengths(1),link_lengths(2),link_lengths(3)), J_psinv_32(th_out(1,i),th_out(2,i),th_out(3,i),link_lengths(1),link_lengths(2),link_lengths(3))  ];
            % Compute joint angles and store for plotting
            dtheta = J_psinv_num*traj_vel(:,i);
            th_out(:,i+1) = th_out(:,i) + dtheta*dt;
        end
        
    end
    %------------- END OF CODE --------------
end

function x = newton_method(anony_f,x0,tol,max_itr)
    % newton_method - Simple implementation of Newton's method for solving
    %                   for the roots of real-valued functions
    %
    % Syntax:  x = newton_method(anony_f,x0,tolerance)
    %
    % Inputs:
    %    anony_f    - anonymous function to be solved using Newton's method
    %                   i.e. x^2 + x = 1 becomes f = @(x) x^2 + x - 1
    %    x0         - initial guess for value
    %    tol        - tolerance to control precision of answer i.e. how
    %                   close final value is to zero
    %    max_itr    - maximum number of iterations before stopping method
    %
    % Outputs:
    %    x          - root of the given function
    %
    % Example:
    %    func = @(x) x^2 + x - 1
    %    root = newton_method(func,1.0,1e-6);
    %
    % Other m-files required: none
    % Subfunctions:           num_dfdx
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Initialize error and guess for value of root
    err_arr = Inf(1,max_itr);
    x_arr = zeros(1,max_itr);
    x_arr(1) = x0;
    % Store all error and root values in array 
    for i = 1:max_itr-1
        x_arr(i+1) = x_arr(i) - anony_f(x_arr(i))/num_dfdx(anony_f,x_arr(i));
        err_arr(i+1) = anony_f(x_arr(i+1));
        % Stop computation if error is below tolerance value
        if abs(err_arr(i+1)) < tol
            break;
        end
    end
    
    % Return root with the smallest error i.e. closest to zero
    [err,indx] = min(abs(err_arr));
    x = x_arr(indx);
    if abs(err) > tol
        fprintf(['Newton''s method did not go below tolerance, err = ',num2str(err),'\n'])
        fprintf('Starting waypoint too close to area outside of workspace\n')
    end  
    %------------- END OF CODE --------------
end

function dfdx = num_dfdx(f, x_input)
    % num_dfdx - Approximates the derivative of a real-valued function y(x) 
    %               at a specified x numerically
    %
    % Syntax:  dfdx = num_dfdx(f, x_input)
    %
    % Inputs:
    %    f          - anonymous function for approximating derivative
    %                   i.e. x^2 + x = 1 becomes f = @(x) x^2 + x - 1
    %    x_input    - x values at which the derivative is evaluated
    %
    % Outputs:
    %    dfdx       - numerical derivative of the function f(x) at x_input
    %
    % Example:
    %    dfdx_num = num_dfdx(anony_f,x);
    %
    % Other m-files required: none
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    % A small constant delta to be used to calculate the derivative
    delta = 1e-12;
     
	% Create a small domain of x and y values around x_input
    x_a = x_input - delta;
    x_b = x_input + delta;
    y_a = f(x_a);
    y_b = f(x_b);
     
    % Calculate the derivative of the function at x_input by computing
    % rise over run
    dfdx = (y_b - y_a)/(x_b - x_a);
    %------------- END OF CODE --------------
end
