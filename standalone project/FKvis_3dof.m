function FKvis_3dof(th_rad, link_lengths, plt_intrvl,ax_h)
    % FKvis_3dof - Takes the joint angles (usually computed by
    %               IKsolver_3dof function) and uses them to plot robot 
    %               joints as circles and links as lines
    %
    % Syntax:  FKvis_3dof(th_rad, link_lengths, plt_intrvl,ax_h)
    %
    % Inputs:
    %    th_rad         - joint angle values for every timestep in radians
    %                       as column vectors i.e. [th1_1 th1_2 ...;
    %                       th2_1 th2_2 ...; th3_1 th3_2 ...]
    %    link_lengths   - length of robot links as 1D row i.e. [l1 l2 l3]
    %    plt_intrvl     - integer that determines frequency of plotting
    %                       otherwise takes forever to plot robot
    %                       traversing entire trajectory
    %    ax_h           - axis handle for visualization
    %
    % Outputs:
    %    None
    %
    % Example:
    %    fig = figure();
    %    ax = axes(fig);
    %    link_lens = [l1 l2 l3];
    %    joint_angles = [th1_1 th1_2 ...; th2_1 th2_2 ...; th3_1 th3_2 ...];
    %    FKvis_3dof(joint_angles, link_lens, 100, ax)
    %
    % Other m-files required: none
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Based on 
    % https://en.wikibooks.org/wiki/Robotics_Kinematics_and_Dynamics/Serial_Manipulator_Position_Kinematics
    % first row always zero cause position of 0th joint is always at origin
    joint_pos_func = @(th1,th2,th3,l1,l2,l3) ...
                    [   0                                                        , 0                                                         ;
                        l1*cos(th1)                                              , l1*sin(th1)                                               ;
                        l1*cos(th1) + l2*cos(th1 + th2)                          , l1*sin(th1) + l2*sin(th1 + th2)                           ;
                        l1*cos(th1) + l2*cos(th1 + th2) + l3*cos(th1 + th2 + th3), l1*sin(th1) + l2*sin(th1 + th2) + l3*sin(th1 + th2 + th3)];

    % Declare plot objects for later use
    h_lines = repelem(line(ax_h,[0 0],[0 0]),size(link_lengths,2));
    h_joints = repelem(line(ax_h,[0 0],[0 0]),size(link_lengths,2));
    h_endeff = line(ax_h,[0 0],[0 0]);
    
    % Flag for error if trajectory happens to pass areas not within
    % reachable workspace of robot
    delete_flag = false;
    for i = 1:size(th_rad,2)
        % check if joint angles are NaN which would indicate trajectory
        % point was not within reachable workspace of robot
        if isnan(th_rad(1,i))
            xy_of_joints = joint_pos_func(th_rad(1,i-1),th_rad(2,i-1),th_rad(3,i-1),link_lengths(1),link_lengths(2),link_lengths(3));
            delete_flag = true;
        else
            xy_of_joints = joint_pos_func(th_rad(1,i),th_rad(2,i),th_rad(3,i),link_lengths(1),link_lengths(2),link_lengths(3));
        end

        % Visualize robot's configuration at certain intervals
        if mod(i,plt_intrvl) == 0 || delete_flag == true
            
            % Draw links as lines, joints as circles and end-effector as
            % hexagram
            for j = 1:size(link_lengths,2)
                h_lines(j) = line(ax_h,[xy_of_joints(j,1),xy_of_joints(j+1,1)],[xy_of_joints(j,2),xy_of_joints(j+1,2)], 'Color', 'k', 'Tag', 'link');
                h_joints(j) = plot(ax_h,xy_of_joints(j,1),xy_of_joints(j,2), 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k', 'MarkerSize', 10, 'Tag', 'joint');
                if j == size(link_lengths,2)
                    h_endeff = plot(ax_h,xy_of_joints(j+1,1),xy_of_joints(j+1,2), 'h', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k', 'MarkerSize', 10, 'Tag', 'endeff');
                end
            end
            
            % Stop visualization if trajectory passes through invalid area
            if delete_flag == true
                title('Trajectory passes through area not in reachable workspace', 'FontSize', 15);
                fprintf('No solution possible, stopping visualizer at last reachable point on trajectory\n')
                return;
            else

                if i == size(th_rad,2)
                    title(ax_h,'Simulation complete', 'FontSize', 15);
                else
                    h_title = title(['phase number = ', num2str(i)], 'FontSize', 15);
                end
                
                pause(0.1);
                % Delete titles, lines and circles from plot to give impression of
                % animated motion
                if i ~= size(th_rad,2)
                    delete(h_title);
                    for j = 1:size(link_lengths,2)
                        delete(h_lines(j));
                        delete(h_joints(j));
                        if j == size(link_lengths,2)
                            delete(h_endeff);
                        end
                    end
                end     
            end
            
        end
    end
    %------------- END OF CODE --------------
end