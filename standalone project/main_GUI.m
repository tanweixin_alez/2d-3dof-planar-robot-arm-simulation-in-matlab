close all;
clear;

pause on;
% Add the UI components
h_cmpnts = setup_gui;

% Make figure visible after adding components
h_cmpnts.fig.Visible = 'on';

function hndl_strct = setup_gui
    % setup_gui - Sets up graph, input field and buttons in graphical user
    %               interface
    %
    % Syntax:  hndl_cmpnts = setup_gui
    %
    % Inputs:
    %    None
    %
    % Outputs:
    %    hndl_strct     - Structure containing the handles of all GUI
    %                       components
    %
    % Example:
    %    h_cmpnts = setup_gui;
    %    h_cmpnts.fig.Visible = 'on';
    %
    % Other m-files required:   none
    % Subfunctions:             linkmenu_callback,
    %                           wayptno_callback,
    %                           wayptbttn_callback,
    %                           trajbttn_callback,
    %                           simbttn_callback,
    %                           demo1bttn_callback,
    %                           demo2bttn_callback,
    %                           demo3bttn_callback,
    %                           setup_ax
    % MAT-files required:       none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Set up figure and plot axis before saving handles in struct
    hndl_strct.fig = figure('Visible','off',...
                             'Position',[300,0,1024,768]);
    hndl_strct.ax = axes(hndl_strct.fig,'Position',[0.1 0.1 0.6 0.8],'Tag','ax');
    hold(hndl_strct.ax,'on'); 
    % Clear plot axis before re-plotting robot workspace and setting plot
    % axis limits
    setup_ax(ones(1,3));
    % Prompt user to act using plot title
    title(hndl_strct.ax,'Please select link lengths', 'FontSize', 15);

    % Set up text explaining the popup menu
    hndl_strct.link_txt = uicontrol(   hndl_strct.fig,'Position',[800 330 150 50],...
                                        'Style','text',...
                                        'String','Link lengths',...
                                        'FontSize',15,...
                                        'Tag','link_txt');
	% Set up pop up menu for determining robot link lengths and link it to 
    % its respective callback function
    hndl_strct.link_menu = uicontrol(   hndl_strct.fig,'Position',[800 250 150 100],...
                                        'Style','popupmenu',...
                                        'String',{'','[1.0,1.0,1.0]','[1.0,0.6,0.3]'},...
                                        'Fontsize',15,...
                                        'Callback',@linkmenu_callback,...
                                        'Tag','link_menu');

    % Set up text explaining the input field
    hndl_strct.waypt_txt = uicontrol(   hndl_strct.fig,'Position',[765 250 225 50],...
                                        'Style','text',...
                                        'String','Input number of waypoints',...
                                        'FontSize',12,...
                                        'Tag','link_txt');
    % Set up input field for specifying number of trajectory waypoints and 
    % button for selecting them in plot axis and link them to their
    % respective callback functions
    hndl_strct.waypt_no = uicontrol(    hndl_strct.fig,'Position',[850 250 50 25],...
                                        'Style','edit',...
                                        'Fontsize',12,...
                                        'Callback',@wayptno_callback,...
                                        'Tag','waypt_no');              
    hndl_strct.waypt_bttn = uicontrol(  hndl_strct.fig,'Position',[765 190 225 50],...
                                        'Style','pushbutton',...
                                        'String','Select waypoints',...
                                        'Fontsize',15,...
                                        'Callback',@wayptbttn_callback,...
                                        'Interruptible','off',...
                                        'Tag','waypt_bttn');
    
	% Set up button for generating trajectory and link it to its respective
	% callback function
    hndl_strct.traj_bttn = uicontrol(   hndl_strct.fig,'Position',[765 130 225 50],...
                                        'Style','pushbutton',...
                                        'String','Generate trajectory',...
                                        'Fontsize',15,...
                                        'Callback',@trajbttn_callback,...
                                        'Interruptible','off',...
                                        'Tag','traj_bttn');
    
	% Set up button for starting simulation with manual inputs and link it 
    % to its respective callback function, HTML syntax used in String
    % property to center text
    hndl_strct.sim_bttn = uicontrol(    hndl_strct.fig,'Position',[765 45 225 75],...
                                        'Style','pushbutton',...
                                        'Max',2,'String','<html><tr><td width=9999 align=center>Simulate 3DoF<br>planar robot',...
                                        'Fontsize',15,...
                                        'Callback',@simbttn_callback,...
                                        'Interruptible','off',...
                                        'Tag','sim_bttn');
    
    % Set up buttons for demos and and link them to their respective
    % callback functions, HTML syntax used in String property to center
    % text
    hndl_strct.demo1_bttn = uicontrol(  hndl_strct.fig,'Position',[765 580 225 50],...
                                        'Style','pushbutton',...
                                        'String','Normal demo',...
                                        'Fontsize',15,...
                                        'Callback',@demo1bttn_callback,...
                                        'Interruptible','off',...
                                        'Tag','demo1_bttn');
    hndl_strct.demo2_bttn = uicontrol(  hndl_strct.fig,'Position',[765 505 225 55],...
                                        'Style','pushbutton',...
                                        'String','<html><tr><td width=9999 align=center>Out of workspace<br>demo',...
                                        'Fontsize',15,...
                                        'Callback',@demo2bttn_callback,...
                                        'Interruptible','off',...
                                        'Tag','demo2_bttn');
    hndl_strct.demo3_bttn = uicontrol( hndl_strct.fig,'Position',[765 430 225 55],...
                                        'Style','pushbutton',...
                                        'String','<html><tr><td width=9999 align=center>Bad initialization<br>demo',...
                                        'Fontsize',15,...
                                        'Callback',@demo3bttn_callback,...
                                        'Interruptible','off',...
                                        'Tag','demo3_bttn');
    %------------- END OF CODE --------------
end

function linkmenu_callback(obj_h,evnt_h)
    % linkmenu_callback - Callback function for the popup menu that
    %                       controls selection of robot link lengths
    %
    % Syntax:  @(obj_h,evnt_h) linkmenu_callback(obj_h,evnt_h)
    %
    % Inputs:
    %    obj_h      - Handle of the GUI object that triggered the callback
    %    evnt_h     - Handle that contains information about specific mouse
    %                   or keyboard actions (unused)
    %
    % Outputs:
    %    None
    %
    % Example:
    %     hndl_strct.link_menu = uicontrol(   hndl_strct.fig,...
    %                                         'Position',[x y width height]
    %                                         'Style','popupmenu',...
    %                                         'String',{'str1','str2'},...
    %                                         'FontSize',15,...
    %                                         'Callback',@linkmenu_callback,...
    %                                         'Tag','link_menu');
    %
    % Other m-files required: none
    % Subfunctions:           setup_ax
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    % Return handle of plot axis
    h_ax = findobj('Tag','ax');
    
    % Check if popup menu selection was valid and sets user data property
    % of popup menu object to selection before displaying choice using
    % title of plot axis
    if obj_h.Value == 1
        title(h_ax,'Please select valid link lengths', 'FontSize', 15);
    elseif obj_h.Value == 2
        obj_h.UserData.link_lens = [1.0,1.0,1.0];
        title(h_ax,'Link lengths [1.0,1.0,1.0] selected', 'FontSize', 15);
    elseif obj_h.Value == 3
        obj_h.UserData.link_lens = [1.0,0.6,0.3];
        title(h_ax,'Link lengths [1.0,0.6,0.3] selected', 'FontSize', 15);
    end
    
    % Clear plot axis before re-plotting robot workspace and setting plot
    % axis limits
    setup_ax(obj_h.UserData.link_lens);
    %------------- END OF CODE --------------
end

function wayptno_callback(obj_h,evnt_h)
    % wayptno_callback - Callback function for the input field that 
    %                       controls number of trajectory waypoints
    %
    % Syntax:  @(obj_h,evnt_h) wayptno_callback(obj_h,evnt_h)
    %
    % Inputs:
    %    obj_h      - Handle of the GUI object that triggered the callback
    %    evnt_h     - Handle that contains information about specific mouse
    %                   or keyboard actions (unused)
    %
    % Outputs:
    %    None
    %
    % Example:
    % hndl_strct.waypt_no = uicontrol(    hndl_strct.fig,
    %                                     'Position',[x y width height],...
    %                                     'Style','edit',...
    %                                     'Fontsize',12,...
    %                                     'Callback',@wayptno_callback,...
    %                                     'Tag','waypt_no');              
    %
    % Other m-files required: none
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Return handle of plot axis
    h_ax = findobj('Tag','ax');
    
    % Set user data property of input field to input rounded to nearest
    % integer value
    obj_h.UserData.no_of_waypts = round(str2double(obj_h.String));
    
    % Check if input value is valid, displays error or input using title
    % of plot axis
    if isnan(obj_h.UserData.no_of_waypts)
        obj_h.UserData.no_of_waypts = 2;
        title(h_ax,'Input is not a number, please try again', 'FontSize', 15);
    elseif obj_h.UserData.no_of_waypts < 2
        title(h_ax,'Number of waypoints must be at least 2, please try again', 'FontSize', 15);
    else
        title(h_ax,['Number of waypoints set to ',num2str(obj_h.UserData.no_of_waypts)], 'FontSize', 15);
    end
    %------------- END OF CODE --------------
end

function wayptbttn_callback(obj_h,evnt_h)
    % wayptbttn_callback - Callback function for button that allows
    %                       graphical selection of the trajectory waypoints
    %
    % Syntax:  @(obj_h,evnt_h) wayptbttn_callback(obj_h,evnt_h)
    %
    % Inputs:
    %    obj_h      - Handle of the GUI object that triggered the callback
    %    evnt_h     - Handle that contains information about specific mouse
    %                   or keyboard actions (unused)
    %
    % Outputs:
    %    None
    %
    % Example:
    % hndl_strct.waypt_bttn = uicontrol(  hndl_strct.fig,
    %                                     'Position',[x y width height],...
    %                                     'Style','pushbutton',...
    %                                     'String','Select waypoints',...
    %                                     'Fontsize',15,...
    %                                     'Callback',@wayptbttn_callback,...
    %                                     'Tag','waypt_bttn');
    %
    % Other m-files required: none
    % Subfunctions:           setup_ax
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------

    % Return handles of plot axis, link legnth popup menu, waypoint number
    % input field and trajectory generation button
    h_ax = findobj('Tag','ax');
    h_linkmenu = findobj('Tag','link_menu');
    h_wayptno = findobj('Tag','waypt_no');
    h_trajbttn = findobj('Tag','traj_bttn');
       
    % Check if relevant user data properties exist and that their valus
    % are valid otherwise display error using title of plot axis
    if isfield(h_linkmenu.UserData,'link_lens') == false
        title(h_ax,'Please specify link lengths', 'FontSize', 15);
        return;
    elseif isfield(h_wayptno.UserData,'no_of_waypts') == false
        title(h_ax,'Please specify number of waypoints', 'FontSize', 15);
        return;
    elseif h_wayptno.UserData.no_of_waypts < 2
        title(h_ax,'Number of waypoints must be at least 2, please try again', 'FontSize', 15);
        return;
    end
    
    % Remove any previous trajectory generated from previous waypoints
    if isfield(h_trajbttn.UserData,'traj_coord') == true
        h_trajbttn.UserData = rmfield(h_trajbttn.UserData,'traj_coord');
    end
    
    % Clear plot axis before re-plotting robot workspace and setting plot
    % axis limits
    setup_ax(h_linkmenu.UserData.link_lens);
    
    % Use ginput() to save user mouse clicks as waypoints and display
    % status using title of plot axis
    obj_h.UserData.waypt_coord = zeros(h_wayptno.UserData.no_of_waypts,2);
    n = 0;
    title(h_ax,{'Please place waypoint,','valid workspace is between the magenta lines'}, 'FontSize', 15);
    while n < h_wayptno.UserData.no_of_waypts
        [x,y,click_flag1] = ginput(1);
        if isempty(x) || click_flag1(1) ~= 1
            break;
        end

        % Prevent points not within the robot's workspace from being 
        % selected
        if (x^2 + y^2) > (sum(h_linkmenu.UserData.link_lens)^2) || ...
        ( (sum(h_linkmenu.UserData.link_lens(2:end)) < h_linkmenu.UserData.link_lens(1)) && ...
        ((x^2 + y^2) < ((h_linkmenu.UserData.link_lens(1)-sum(h_linkmenu.UserData.link_lens(2:end)))^2)) )
            title(h_ax,'Waypoint is not in valid workspace, please try again', 'FontSize', 15);
            fprintf(['Current no. of input waypoints: ',num2str(n),'\n'])
        else
            % Update user on status of graphical input process
            n = plus(n,1);
            title(h_ax,'Press Enter if you want to terminate waypoint input earlier', 'FontSize', 15);
            fprintf(['Current no. of input waypoints: ',num2str(n),'\n'])
            obj_h.UserData.waypt_coord(n,:) = [x,y];
            plot(h_ax,x,y,'rx','LineWidth',2)
            drawnow
        end
    end
    title(h_ax,'All waypoints have been successfully placed', 'FontSize', 15);
    %------------- END OF CODE --------------
end

function trajbttn_callback(obj_h,evnt_h)
    % trajbttn_callback - Callback function for the button that generates
    %                       cubic hermite spline from trajectory waypoints
    %
    % Syntax:  @(obj_h,evnt_h) trajbttn_callback(obj_h,evnt_h)
    %
    % Inputs:
    %    obj_h      - Handle of the GUI object that triggered the callback
    %    evnt_h     - Handle that contains information about specific mouse
    %                   or keyboard actions (unused)
    %
    % Outputs:
    %    None
    %
    % Example:
    % hndl_strct.traj_bttn = uicontrol(   hndl_strct.fig,...
    %                                     'Position',[x y width height],...
    %                                     'Style','pushbutton',...
    %                                     'String','Generate trajectory',...
    %                                     'Fontsize',15,...
    %                                     'Callback',@trajbttn_callback,...
    %                                     'Tag','traj_bttn');
    %
    % Other m-files required: cubicHermSpline.m
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
  
    % Return handles of plot axis, waypoint number input field and 
    % waypoint selection button
    h_ax = findobj('Tag','ax');
    h_wayptno = findobj('Tag','waypt_no');
    h_wayptbttn = findobj('Tag','waypt_bttn');
       
    % Check if relevant user data properties exist and that their valus
    % are valid otherwise display error using title of plot axis
    if isfield(h_wayptno.UserData,'no_of_waypts') == false
        title(h_ax,'Please specify number of waypoints', 'FontSize', 15);
        return;
    elseif h_wayptno.UserData.no_of_waypts < 2
        title(h_ax,'Number of waypoints must be at least 2, please try again', 'FontSize', 15);
        return;
    elseif isfield(h_wayptbttn.UserData,'waypt_coord') == false
        title(h_ax,'Please select waypoints in plot', 'FontSize', 15);
        return;
    end
    
    % Generate trajectory from waypoints obtained in wayptbttn_callback
    % and display it in plot axis
    obj_h.UserData.traj_coord = cubicHermSpline(h_wayptno.UserData.no_of_waypts,h_wayptbttn.UserData.waypt_coord,1e3);    
    plot(h_ax,obj_h.UserData.traj_coord(1,:),obj_h.UserData.traj_coord(2,:),'b')
    title(h_ax,'Trajectory successfully generated', 'FontSize', 15);
    %------------- END OF CODE --------------
end

function simbttn_callback(obj_h,evnt_h)
    % simbttn_callback - Callback function for the button that starts
    %                       simulation of robot end-effector following
    %                       trajectory path
    %
    % Syntax:  @(obj_h,evnt_h) simbttn_callback(obj_h,evnt_h)
    %
    % Inputs:
    %    obj_h      - Handle of the GUI object that triggered the callback
    %    evnt_h     - Handle that contains information about specific mouse
    %                   or keyboard actions (unused)
    %
    % Outputs:
    %    None
    %
    % Example:
    % hndl_strct.sim_bttn = uicontrol(    hndl_strct.fig,
    %                                     'Position',[x y width height],...
    %                                     'Style','pushbutton',...
    %                                     'String','Start simulation',...
    %                                     'Fontsize',15,...
    %                                     'Callback',@simbttn_callback,...
    %                                     'Interruptible','off',...
    %                                     'Tag','sim_bttn');
    %
    % Other m-files required: IKsolver_3dof.m, FKvis_3dof.m
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
 
    % Return handles of plot axis, link length popup menu, waypoint 
    % selection button and trajectory generation button
    h_ax = findobj('Tag','ax');
    h_linkmenu = findobj('Tag','link_menu');
    h_trajbttn = findobj('Tag','traj_bttn');
       
    % Check if relevant user data properties exist and that their values
    % are valid otherwise display error using title of plot axis
    if isfield(h_linkmenu.UserData,'link_lens') == false
        title(h_ax,'Please specify link lengths', 'FontSize', 15);
        return;
    elseif isfield(h_trajbttn.UserData,'traj_coord') == false
        title(h_ax,'Please generate trajectory', 'FontSize', 15);
        return;
    end
    
    % Remove any previous visualization of robot arm from plot axis
    h_links = findobj(h_ax,'Type','line','Tag','link');
    h_joints = findobj(h_ax,'Type','line','Tag','joint');
    h_endeff = findobj(h_ax,'Type','line','Tag','endeff');
    if size(h_links,1) > 0
        for i = 1:size(h_links,1)
            delete(h_links(i));
            delete(h_joints(i));
        end
        delete(h_endeff);
    end
    
    % Determine the joint angles of the robot based on trajectory
    % coordinates
    obj_h.UserData.joint_angles_rad = IKsolver_3dof(h_trajbttn.UserData.traj_coord,h_linkmenu.UserData.link_lens);

    % Visualize the robot's motion based on the computed joint angles
    FKvis_3dof(obj_h.UserData.joint_angles_rad,h_linkmenu.UserData.link_lens,50,h_ax);
    %------------- END OF CODE --------------
end

function demo1bttn_callback(obj_h,evnt_h)
    % demo1bttn_callback - Callback function for the button that starts
    %                       normal demo simulation
    %
    % Syntax:  @(obj_h,evnt_h) demo1bttn_callback(obj_h,evnt_h)
    %
    % Inputs:
    %    obj_h      - Handle of the GUI object that triggered the callback
    %    evnt_h     - Handle that contains information about specific mouse
    %                   or keyboard actions (unused)
    %
    % Outputs:
    %    None
    %
    % Example:
    % hndl_strct.demo1_bttn = uicontrol(  hndl_strct.fig,
    %                                     'Position',[x y width height],...
    %                                     'Style','pushbutton',...
    %                                     'String','Normal demo',...
    %                                     'Fontsize',15,...
    %                                     'Callback',@demo1bttn_callback,...
    %                                     'Interruptible','off',...
    %                                     'Tag','demo1_bttn');
    %
    % Other m-files required: none
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Return handle of plot axis
    h_ax = findobj('Tag','ax');
    
    % Return handle of link length popup menu and replace selection with
    % appropriate one before calling its callback function
    h_linkmenu = findobj('Tag','link_menu');
    h_linkmenu.Value = 2;
    feval(h_linkmenu.Callback,h_linkmenu);
    
    % Return handle of waypoint number input field and fill field with
    % appropriate number
    h_wayptno = findobj('Tag','waypt_no');
    h_wayptno.String = '9';
    h_wayptno.UserData.no_of_waypts = 9;
    
    % Return handle of waypoint selection button and replace user data
    % property with appropriate waypoint coordinates before plotting them
    % on plot axis
    h_wayptbttn = findobj('Tag','waypt_bttn');
    h_wayptbttn.UserData.waypt_coord = [0 2; -1 1; 0 0; 1 -1; 0 -2; -1 -1; 0 0; 1 1; 0 2];
    plot(h_ax,h_wayptbttn.UserData.waypt_coord(:,1),h_wayptbttn.UserData.waypt_coord(:,2),'rx','LineWidth',2)
    
    % Return handle of trajectory generation button and call its callback 
    % function
    h_trajbttn = findobj('Tag','traj_bttn');
    feval(h_trajbttn.Callback,h_trajbttn);

    % Return handle of simulation button and call its callback function
    h_simbttn = findobj('Tag','sim_bttn');
    feval(h_simbttn.Callback,h_simbttn);
    %------------- END OF CODE --------------
end

function demo2bttn_callback(obj_h,evnt_h)
    % demo2bttn_callback - Callback function for the button that starts
    %                       out of workspace demo simulation
    %
    % Syntax:  @(obj_h,evnt_h) demo2bttn_callback(obj_h,evnt_h)
    %
    % Inputs:
    %    obj_h      - Handle of the GUI object that triggered the callback
    %    evnt_h     - Handle that contains information about specific mouse
    %                   or keyboard actions (unused)
    %
    % Outputs:
    %    None
    %
    % Example:
    % hndl_strct.demo2_bttn = uicontrol(  hndl_strct.fig,
    %                                     'Position',[x y width height],...
    %                                     'Style','pushbutton',...
    %                                     'String','Out of workspace demo',...
    %                                     'Fontsize',15,...
    %                                     'Callback',@demo2bttn_callback,...
    %                                     'Interruptible','off',...
    %                                     'Tag','demo2_bttn');
    %
    % Other m-files required: none
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Return handle of plot axis
    h_ax = findobj('Tag','ax');
    
    % Return handle of link length popup menu and replace selection with
    % appropriate one before calling its callback function
    h_linkmenu = findobj('Tag','link_menu');
    h_linkmenu.Value = 3;
    feval(h_linkmenu.Callback,h_linkmenu);
    
    % Return handle of waypoint number input field and fill field with
    % appropriate number
    h_wayptno = findobj('Tag','waypt_no');
    h_wayptno.String = '4';
    h_wayptno.UserData.no_of_waypts = 4;
    
    % Return handle of waypoint selection button and replace user data
    % property with appropriate waypoint coordinates before plotting them
    % on plot axis
    h_wayptbttn = findobj('Tag','waypt_bttn');
    h_wayptbttn.UserData.waypt_coord = [-1 1; 1 1; -1 -1; 1 -1];
    plot(h_ax,h_wayptbttn.UserData.waypt_coord(:,1),h_wayptbttn.UserData.waypt_coord(:,2),'rx','LineWidth',2)
    
    % Return handle of trajectory generation button and call its callback 
    % function
    h_trajbttn = findobj('Tag','traj_bttn');
    feval(h_trajbttn.Callback,h_trajbttn);
    
    % Return handle of simulation button and call its callback function
    h_simbttn = findobj('Tag','sim_bttn');
    feval(h_simbttn.Callback,h_simbttn);
    %------------- END OF CODE --------------
end

function demo3bttn_callback(obj_h,evnt_h)
    % demo3bttn_callback - Callback function for the button that starts
    %                       nad initialization demo simulation
    %
    % Syntax:  @(obj_h,evnt_h) demo3bttn_callback(obj_h,evnt_h)
    %
    % Inputs:
    %    obj_h      - Handle of the GUI object that triggered the callback
    %    evnt_h     - Handle that contains information about specific mouse
    %                   or keyboard actions (unused)
    %
    % Outputs:
    %    None
    %
    % Example:
    % hndl_strct.demo3_bttn = uicontrol(  hndl_strct.fig,
    %                                     'Position',[x y width height],...
    %                                     'Style','pushbutton',...
    %                                     'String','Bad initialization demo',...
    %                                     'Fontsize',15,...
    %                                     'Callback',@demo3bttn_callback,...
    %                                     'Interruptible','off',...
    %                                     'Tag','demo3_bttn');
    %
    % Other m-files required: none
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Return handle of plot axis
    h_ax = findobj('Tag','ax');
    
    % Return handle of link length popup menu and replace selection with
    % appropriate one before calling its callback function
    h_linkmenu = findobj('Tag','link_menu');
    h_linkmenu.Value = 3;
    feval(h_linkmenu.Callback,h_linkmenu);
    
    % Return handle of waypoint number input field and fill field with
    % appropriate number
    h_wayptno = findobj('Tag','waypt_no');
    h_wayptno.String = '2';
    h_wayptno.UserData.no_of_waypts = 2;
    
    % Return handle of waypoint selection button and replace user data
    % property with appropriate waypoint coordinates before plotting them
    % on plot axis
    h_wayptbttn = findobj('Tag','waypt_bttn');
    h_wayptbttn.UserData.waypt_coord = [-0.3 0.3; 1 1];
    plot(h_ax,h_wayptbttn.UserData.waypt_coord(:,1),h_wayptbttn.UserData.waypt_coord(:,2),'rx','LineWidth',2)
    
    % Return handle of trajectory generation button and call its callback 
    % function
    h_trajbttn = findobj('Tag','traj_bttn');
    feval(h_trajbttn.Callback,h_trajbttn);
    
    % Return handle of simulation button and call its callback function
    h_simbttn = findobj('Tag','sim_bttn');
    feval(h_simbttn.Callback,h_simbttn);
    %------------- END OF CODE --------------
end

function setup_ax(link_lengths)
    % setup_ax - Clears plot axis and sets up axis limits as well as plots
    %               limits of robot workspace
    %
    % Syntax:  setup_ax(link_lengths)
    %
    % Inputs:
    %    link_lengths   - length of robot links as 1D row i.e. [l1 l2 l3]
    %
    % Outputs:
    %    None
    %
    % Example:
    %   fig = figure();
    %   ax = axes(fig,'Tag','ax');
    %   link_lens = [l1 l2 l3];
    %   setup_ax(link_lens);
    %
    % Other m-files required: none
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    
    % Return handle of plot axis and clears it
    h_ax = findobj('Tag','ax');
    cla(h_ax);
    
    % Set up appropriate axis limits for plot axis based on sum of link
    % lengths rounded up to largest integer
    x_limits = [-ceil(sum(link_lengths)) ceil(sum(link_lengths))];
    xlim(h_ax,x_limits);
    line(h_ax,x_limits,[0 0],'Color','k');
    y_limits = [-ceil(sum(link_lengths)) ceil(sum(link_lengths))];
    ylim(h_ax,y_limits);
    line(h_ax,[0 0],y_limits,'Color','k');
    
    % Determine robot workspace and display in plot axis
    % In case of 2D planar robot, workspace is in a shape of a circle
    th = linspace(0,2*pi,100);
    crc_outer = [sum(link_lengths) * cos(th); sum(link_lengths) * sin(th)];
    plot(h_ax,crc_outer(1,:),crc_outer(2,:),'m')
    % Inner workspace limits only applicable when link2 + link3 < link1
    % which results in a donut shape
    if sum(link_lengths(2:end)) < link_lengths(1)
        r_inner = link_lengths(1)-sum(link_lengths(2:end));
        crc_inner = [r_inner * cos(th); r_inner * sin(th)];
        plot(h_ax,crc_inner(1,:),crc_inner(2,:),'m')
    end
    
    grid(h_ax,'on');
    axis(h_ax,'equal');
    %------------- END OF CODE --------------
end