function spline_vals = cubicHermSpline(no_of_waypts, waypt_coord, no_of_interp_pts)
    % cubicHermSpline - Generates a cubic hermite spline with C-2 continuity
    %                   from a set of xy coordinates i.e. waypoints
    % Based on http://erikerlandson.github.io/blog/2013/03/16/smooth-gradients-for-cubic-hermite-splines/
    %
    % Syntax:  spline_vals = cubicHermSpline(no_of_waypts, waypt_coord, no_of_interp_pts)
    %
    % Inputs:
    %    no_of_waypts       - Number of xy coordinate sets
    %    waypt_coord        - xy coordinates as row vectors 
    %                           i.e [x0 y0; x1 y1; ...]
    %    no_of_interp_pts   - Number of interpolation points between
    %                           waypoints, suggested value is 1000 e.g. 6
    %                           waypoints would be (6-1)*1000 = 5000 points
    %                           in total
    %
    % Outputs:
    %    spline_vals        - xy coordinates of spline path as column vectors
    %                           i.e. [x0 x1 ...; y0 y1 ...]
    %
    % Example:
    %    waypts = [x0 y0; x1 y1];
    %    spline = cubicHermSpline(size(waypts,1), waypts, 1000);
    %
    % Other m-files required: none
    % Subfunctions:           none
    % MAT-files required:     none
    %
    % See also: none
    % Author: TAN Wei Xin, Cybernetics and Robotics
    % Czech Technical University in Prague, FEL
    % email address: tanweixi@fel.cvut.cz
    % Website: none
    % February 2019; Last revision: 24-April-2019

    %------------- BEGIN CODE --------------
    % Set up system of equations from waypoint coordinates
    A_mat = full(gallery('tridiag',no_of_waypts,2,8,2));
    A_mat(1,1) = 4;
    A_mat(end,end) = 4;
    b_x_vec = zeros(no_of_waypts,1);
    b_y_vec = zeros(no_of_waypts,1);
    for i = 1:no_of_waypts
        if i == 1
            b_x_vec(i) = 6*(waypt_coord(i+1,1)-waypt_coord(i,1));
            b_y_vec(i) = 6*(waypt_coord(i+1,2)-waypt_coord(i,2));
        elseif i == no_of_waypts
            b_x_vec(i) = 6*(waypt_coord(i,1)-waypt_coord(i-1,1));
            b_y_vec(i) = 6*(waypt_coord(i,2)-waypt_coord(i-1,2));
        else
            b_x_vec(i) = 6*(waypt_coord(i+1,1)-waypt_coord(i-1,1));
            b_y_vec(i) = 6*(waypt_coord(i+1,2)-waypt_coord(i-1,2));
        end
    end
    
    % Solve system of equations to determine velocity vector at each waypoint
    vel_x_cmpnt = A_mat\b_x_vec;
    vel_y_cmpnt = A_mat\b_y_vec;
    waypt_vel_vec = [vel_x_cmpnt,vel_y_cmpnt];

    % Cubic hermite spline equation defined as
    % A*F0(t) + B*F1(t) + a_vec*F2(t) + b_vec*F3(t) for t = 0:1
    F0 = @(t) 2*(t.^3) - 3*(t.^2) + 1;
    F1 = @(t) -2*(t.^3) + 3*(t.^2);
    F2 = @(t) t.^3 - 2*(t.^2) + t;
    F3 = @(t) t.^3 - t.^2;
    
    t_vals = linspace(0.0,1.0,no_of_interp_pts);
    spline_vals = zeros(2,(no_of_waypts-1)*no_of_interp_pts);
    
    % Plug in t_vals to determine coordinates on spline
    for i = 1:(no_of_waypts-1)
        A = transpose(waypt_coord(i,:));
        B = transpose(waypt_coord(i+1,:));
        a_vec = transpose(waypt_vel_vec(i,:));
        b_vec = transpose(waypt_vel_vec(i+1,:));

        spline_vals(:,((i-1)*no_of_interp_pts)+1:i*no_of_interp_pts) = ...
            A*F0(t_vals) + B*F1(t_vals) + a_vec*F2(t_vals) + b_vec*F3(t_vals);
    end
    %------------- END OF CODE --------------
end