function theta_out = ik_2d_3dof(phase_input, dt_input, theta_init, link1_length, link2_length, link3_length)
    syms length1 length2 length3

    syms theta1 theta2 theta3

    syms weight1 weight2 weight3

    theta_desired = [ theta1 ;
                      theta2 ;
                      theta3 ];

    link_length = [ length1 ;
                    length2 ;
                    length3 ];

    no_of_links = size(link_length,1);
    % no_of_phases = 4000;
    no_of_phases = phase_input;
    time_per_phase = dt_input;

    for n = 1:no_of_links
        link_pos(n,1) = link_length(n,1);
        link_pos(n,2) = 0;
    end


    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % Computes the inverse kinematics transformation matrix symbolically and
    % % converts them into inline functions with the specified arguments

    R=@(theta)[ cos(theta) -sin(theta);
                sin(theta)  cos(theta)];

    T = transpose(link_pos(1,:));
    FK_link1 = R(theta_desired(1))*T;
    T = transpose(link_pos(2,:));
    FK_link2 = FK_link1 + R(theta_desired(1))*R(theta_desired(2))*T;
    T = transpose(link_pos(3,:));
    FK_end_eff = FK_link2 + R(theta_desired(1))*R(theta_desired(2))*R(theta_desired(3))*T;

    J = jacobian(FK_end_eff, theta_desired);

    J_psinv = transpose(J)/(J*transpose(J));

    variables = [ theta_desired; link_length ];
    J_psinv_11 = matlabFunction(J_psinv(1,1), 'Vars', variables);
    J_psinv_12 = matlabFunction(J_psinv(1,2), 'Vars', variables);
    J_psinv_21 = matlabFunction(J_psinv(2,1), 'Vars', variables);
    J_psinv_22 = matlabFunction(J_psinv(2,2), 'Vars', variables);
    J_psinv_31 = matlabFunction(J_psinv(3,1), 'Vars', variables);
    J_psinv_32 = matlabFunction(J_psinv(3,2), 'Vars', variables);

%     deg_2_rad = @(deg) (pi/180)*deg;
% 
%     theta1_num = deg_2_rad(theta_init(1));
%     theta2_num = deg_2_rad(theta_init(2));
%     theta3_num = deg_2_rad(theta_init(3));
    
    link_lengths = [link1_length, link2_length, link3_length];
    init_x = 2.0; init_y = 1.5;
    s_str1 = num2str(link_lengths(1));
    s_str2 = strcat(num2str(link_lengths(2)),'*sin(th)');
    for i = 2:no_of_links
        s_str1 = strcat(s_str1,'+',num2str(link_lengths(i)),'*cos(',num2str(i-1),'*th)');
        if i < no_of_links
            s_str2 = strcat(s_str2,'+',num2str(link_lengths(i+1)),'*sin(',num2str(i),'*th)');
        end
    end
    s_str = strcat('(',s_str1,')^2+(',s_str2,')^2-',num2str(init_x^2 + init_y^2));
    s_func = str2func(strcat('@(th)',s_str));
    th_rad_val = newton_method(s_func,1.0,1e-6);
    
    gamma = atan2(init_y,init_x);
    gamma_x = strcat(num2str(link_lengths(1)),'*cos(a)');
    gamma_y = strcat(num2str(link_lengths(1)),'*sin(a)');
     for i = 2:no_of_links
        gamma_x = strcat(gamma_x,'+',num2str(link_lengths(i)),'*cos(a+',num2str(i-1),'*',num2str(th_rad_val),')');
        gamma_y = strcat(gamma_y,'+',num2str(link_lengths(i)),'*sin(a+',num2str(i-1),'*',num2str(th_rad_val),')');
    end
    gamma_str = strcat('atan2(',gamma_y,',',gamma_x,')-',num2str(gamma));
    gamma_func = str2func(strcat('@(a)',gamma_str));
    alpha_rad_val = newton_method(gamma_func,1.0,1e-6);

    theta1_num = alpha_rad_val;
    theta2_num = th_rad_val;
    theta3_num = th_rad_val;
    
    theta_out = zeros(3,no_of_phases);
    theta_out(1,1) = (180/pi)*theta1_num;
    theta_out(2,1) = (180/pi)*theta2_num;
    theta_out(3,1) = (180/pi)*theta3_num;

    length1_num = link1_length;
    length2_num = link2_length;
    length3_num = link3_length;

    % end_eff_init = [length1_num+length2_num+length3_num, 0];
    FK_end_eff_func = matlabFunction(FK_end_eff, 'Vars', variables);
    end_eff_init = FK_end_eff_func(theta1_num,theta2_num,theta3_num,length1_num,length2_num,length3_num);


    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % Specifies targets for the inverse kinematics solver

    end_eff_tar = [ 0, 3 ];
    % end_eff_tar = [ 2, 0.5 ;
    %                 1,   1 ;
    %               0.5,   2 ;
    %                 0,   3 ];

    no_of_tar = size(end_eff_tar, 1);
    phases_btwn_tar = no_of_phases/no_of_tar;

    end_eff_pos = zeros(no_of_phases,2);
    end_eff_pos(1,:) = end_eff_init;
    end_eff_vel = zeros(no_of_phases-1,2);


    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % % Uses linear interpolation for calculating position and velocity to 
    % % specify the trajectory between the initial and target points

    pos_f = @(pos_now,pos_end,pos_init,phases_btwn_pos) pos_now + ((pos_end-pos_init)/(phases_btwn_pos-1));

    vel_f = @(pos_next,pos_now,dt) (pos_next-pos_now)/dt;

    for i = 1:(no_of_phases-1)

        if      i < (1/no_of_tar)*no_of_phases
            end_eff_pos(i+1,:) = pos_f( end_eff_pos(i,:),end_eff_tar(1,:),end_eff_pos(1,:),phases_btwn_tar );
        elseif  i < (2/no_of_tar)*no_of_phases
            end_eff_pos(i+1,:) = pos_f( end_eff_pos(i,:),end_eff_tar(2,:),end_eff_pos((1/4)*no_of_phases,:),phases_btwn_tar );
        elseif  i < (3/no_of_tar)*no_of_phases
            end_eff_pos(i+1,:) = pos_f( end_eff_pos(i,:),end_eff_tar(3,:),end_eff_pos((2/4)*no_of_phases,:),phases_btwn_tar );
        else
            end_eff_pos(i+1,:) = pos_f( end_eff_pos(i,:),end_eff_tar(4,:),end_eff_pos((3/4)*no_of_phases,:),phases_btwn_tar );
        end
        end_eff_vel(i,:) = vel_f( end_eff_pos(i+1,:),end_eff_pos(i,:),time_per_phase );

        J_psinv_num = [   J_psinv_11(theta1_num,theta2_num,theta3_num,length1_num,length2_num,length3_num), J_psinv_12(theta1_num,theta2_num,theta3_num,length1_num,length2_num,length3_num)  ;
                          J_psinv_21(theta1_num,theta2_num,theta3_num,length1_num,length2_num,length3_num), J_psinv_22(theta1_num,theta2_num,theta3_num,length1_num,length2_num,length3_num)  ;
                          J_psinv_31(theta1_num,theta2_num,theta3_num,length1_num,length2_num,length3_num), J_psinv_32(theta1_num,theta2_num,theta3_num,length1_num,length2_num,length3_num)  ];


        % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
        % % Solves the change in angles at every time step and saves them in an
        % % array for plotting in the FK visualizer

        T = transpose(end_eff_vel(i,:));
        dtheta = J_psinv_num*T;


        theta1_num = theta1_num + (dtheta(1)*time_per_phase);
        theta2_num = theta2_num + (dtheta(2)*time_per_phase);
        theta3_num = theta3_num + (dtheta(3)*time_per_phase);

        theta_out(:,i+1) = [ (180/pi)*theta1_num ;
                             (180/pi)*theta2_num ;
                             (180/pi)*theta3_num ];
    end
end

function x = newton_method(anony_f,x0,tolerance)
    err = Inf;
    x = x0;
    % % iterate till err as close to 0 as possible
    while abs(err) > tolerance
       xPrev = x;
       x = xPrev - anony_f(xPrev)/num_dfdx(anony_f,xPrev);
       err = anony_f(x);
       disp(err)
    end
end

function dfdx = num_dfdx(f, x_input)
     
%     Declares a very small delta to be used to calculate the derivative
    delta = 1e-12;
     
%     Creates a small domain of x and y values about the x_input
    x_a = x_input - delta;
    x_b = x_input + delta;
    y_a = f(x_a);
    y_b = f(x_b);
     
%     Calculates the derivative of the function at the point by computing
%     rise over run
    dfdx = (y_b - y_a)/(x_b - x_a);
end
